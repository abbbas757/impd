# impd

[![pipeline status](https://gitlab.com/sj1k/impd/badges/master/pipeline.svg)](https://gitlab.com/sj1k/impd/commits/master)


Itunes Mini - MPD


# Installing

Install `pip3 install impd --user`

It also requires Gtk3 so `package_manager install python3-gi`

# Running

Running `impd --host MPD_HOST --port MPD_PORT`

# Screenshots

(thanks eti)

![wew](https://x.eti.tf/rQnOT.png)

# TODO

- Add help to arguments
- Check if artist - title looks good.
